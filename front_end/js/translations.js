/**
 * INSPINIA - Responsive Admin Theme
 *
 */
function config($translateProvider) {

    $translateProvider
        .translations('en', {

            // Define all menu elements
            HOME: 'Home',
            MAPS: 'Maps',
            LISTMAPS: 'List Maps',
            DASHBOARD: 'Dashboard',
            GRAPHS: 'Graphs',
            MAILBOX: 'Mailbox',
            WIDGETS: 'Widgets',
            METRICS: 'Metrics',
            FORMS: 'Forms',
            APPVIEWS: 'App views',
            OTHERPAGES: 'Other pages',
            UIELEMENTS: 'UI elements',
            MISCELLANEOUS: 'Miscellaneous',
            GRIDOPTIONS: 'Grid options',
            TABLES: 'Tables',
            COMMERCE: 'E-commerce',
            GALLERY: 'Gallery',
            MENULEVELS: 'Menu levels',
            ANIMATIONS: 'Animations',
            LANDING: 'Landing page',
            LAYOUTS: 'Layouts',
            LOGOUT:'Log out',

            //Formulario
            TITLE:'Title',
            MARKERS:'Markers',
            COMMENTS:'Comments',
            POLL:'Poll',
            TAGS:'Tags',
            CANCEL:'Cancel',
            CREATE:'Create',
            OPTION:'Option',
            ADDOPTION:'Add one option',
            TAGSCOMMENT:'Enter keywords separated by commas.',
            ZONE:'Zone',
            PATHSROUTES:'Paths/Routes',
            NEWMAP:'New Map',
            LOCATIONFORPUNTS:'Location for Punts',
            LOCATIONFORHOT:'Location for Heat spots',
            PAINTDESCRIPTION:'As drawn on the map',

            //Titulos
            ALLPROYECTS: 'All the proyects to MapReference',

            // Define some custom text
            WELCOME: 'Welcome Antonio',
            MESSAGEINFO: 'You have 2 comments and 1 edition.',
            SEARCH: 'Search maps...',

        })
        .translations('es', {

            // Define all menu elements
            HOME: 'Inicio',
            MAPS: 'Mapas',
            LISTMAPS: 'Lista de Mapas',
            DASHBOARD: 'Salpicadero',
            GRAPHS: 'Gráficos',
            MAILBOX: 'El correo',
            WIDGETS: 'Widgets',
            METRICS: 'Métrica',
            FORMS: 'Formas',
            APPVIEWS: 'Vistas app',
            OTHERPAGES: 'Otras páginas',
            UIELEMENTS: 'UI elements',
            MISCELLANEOUS: 'Misceláneo',
            GRIDOPTIONS: 'Cuadrícula',
            TABLES: 'Tablas',
            COMMERCE: 'E-comercio',
            GALLERY: 'Galería',
            MENULEVELS: 'Niveles de menú',
            ANIMATIONS: 'Animaciones',
            LANDING: 'Página de destino',
            LAYOUTS: 'Esquemas',
            LOGOUT:'Salir',

            //Formulario
            TITLE:'Titulo',
            MARKERS:'Marcadores',
            COMMENTS:'Comentarios',
            POLL:'Encuesta',
            TAGS:'Etiquetas',
            CANCEL:'Cancelar',
            CREATE:'Crear',
            OPTION:'Opción',
            ADDOPTION:'Añadir una opción',
            TAGSCOMMENT:'Escribe las palabras clave separadas por comas.',
            ZONE:'Zonas',
            PATHSROUTES:'Caminos/Rutas',
            NEWMAP:'Nuevo Mapa',
            LOCATIONFORPUNTS:'Ubicación por Puntos',
            LOCATIONFORHOT:'Ubicación por Mancha de Calor',
            PAINTDESCRIPTION:'Como dibujaras en el mapa',

            // Define some custom text
            WELCOME: 'Bienvenido Antonio',
            MESSAGEINFO: 'Usted tiene 2 comentarios y 1 edición.',
            SEARCH: 'Busca mapas ...',
        });

    $translateProvider.preferredLanguage('es');

}

angular
    .module('inspinia')
    .config(config)
