var express = require('express');
var app = express();

app.use(express.static('front_end'));

var server = app.listen(8080, function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log('En marcha angularjs escuchando en http://%s:%s', host, port);

});